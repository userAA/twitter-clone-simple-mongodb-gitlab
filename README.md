gitlab page https://gitlab.com/userAA/twitter-clone-simple-mongodb-gitlab.git
gitlab twitter-clone-simple-mongodb-gitlab

проект twitter_clone (simple)
используемые технологии на фронтенде:
    @material-ui/core,
    @material-ui/icons,
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    axios,
    jwt-decode,
    react,
    react-dom,
    react-made-with-love,
    react-redux,
    react-router-dom,
    react-scripts,
    redux,
    redux-thunk,
    web-vitals;

используемые технологии на бекэнде:
    bcrypt,
    bcryptjs,
    body-parser,
    cors,
    dotenv,
    express,
    jsonwebtoken,
    mongoose,
    passport,
    passport-jwt,
    validator;

Это простой твиттер клон. 
В этом клоне можно зарегистрировать сколько угодно пользователей, но авторизовывается только один из них. 
Можно менять авторизованного пользователя.
Модель базы данных пользователей содержит в себе их почту логин пароль, массив идентификаторов авторизованных 
и не авторизованных пользователей following, и массив идентификаторов авторизованных пользователей followers. 
Можно найти пользователя по его логину.
Пользователь может создать пост. Модель базы данных поста включает в себя данные пользователя (его почту логин), 
который создал этот пост, текст и идентификатор поста. Можно выводить все посты пользователей или посты которые
создали пользователи с идентификаторами в массивах following моделей баз данных всех зарегистированных пользователей. 